﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 direction;
    public float speed;
 
    void Update()
    {
        direction = ClampVector3(direction);
        transform.Rotate(direction * (speed + Time.deltaTime));
    }

    public static Vector3 ClampVector3(Vector3 target)
    {
        float clampedX = Mathf.Clamp(target.x, 0f, 0f);
        float clampedY = Mathf.Clamp(target.y, 0f, 0f);
        float clampedZ = Mathf.Clamp(target.z, 0f, 0f);
        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);
        return result;
    }
}